Rails.application.routes.draw do
  #resources :posts
  resources :users do
    resources :posts
  end
  get "login", to: redirect("/auth/google_oauth2"), as: "login"
  get "logout", to: "sessions#destroy", as: "logout"
  get "auth/:provider/callback", to: "sessions#create"
  get "auth/failure", to: redirect("/")
  get "auth/required", to: "sessions#new"
  get "mypage", to: "users#mypage"
  get "unauthorized", to: "sessions#unauthorized"
  root to: "posts#facepage"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
