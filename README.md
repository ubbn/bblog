# Xapper

Personal purpose blogging application

## Installation

``` bash
yarn add @rails/actiontext
yarn add trix
bundle install
rails action_text:install
rails db:migrate

# Compile manually 
rails webpacker:compile
```

## Dependencies

* Ruby 2.6+
* Rails 6.+
* Webpack
* Yarn
