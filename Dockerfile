FROM ruby:2.6.0

ADD https://dl.yarnpkg.com/debian/pubkey.gpg /tmp/yarn-pubkey.gpg
RUN apt-key add /tmp/yarn-pubkey.gpg && rm /tmp/yarn-pubkey.gpg
RUN echo 'deb http://dl.yarnpkg.com/debian/ stable main' > /etc/apt/sources.list.d/yarn.list

RUN apt-get update && apt-get install -qq -y --no-install-recommends \
      build-essential libpq-dev curl
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt-get update && apt-get install -qq -y --no-install-recommends nodejs yarn
RUN apt-get install -y git

# Make ssh dir
#RUN mkdir /root/.ssh/
#COPY $HOME/.ssh/id_rsa.pub /root/.ssh/
RUN git clone https://gitlab.com/ubbn/bblog.git /xapper
WORKDIR /xapper

# replace shell with bash so we can source files
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN yarn -v
RUN yarn add @rails/actiontext
RUN yarn add trix
RUN yarn install --check-files
RUN bundle install
RUN rails action_text:install
RUN rails db:migrate

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
