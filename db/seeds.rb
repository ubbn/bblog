# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create([{
  provider: "google_oauth2",
  uid: "108103864061661984333",
  email: "ubbuyan@gmail.com",
  first_name: "Bbn",
  last_name: "U",
  picture: "https://lh3.googleusercontent.com/a-/AAuE7mBinmft57NR15D83y0R14k3nRMewmMHF4Hm_2EKSQ",
  is_admin: true,
}])
