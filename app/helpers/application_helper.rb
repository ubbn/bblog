module ApplicationHelper
  def active_class(link_path)
    current_page?(link_path) ? "active" : ""
  end

  def link_to_unless_current(body, url, classes)
    unless current_page?(url)
      link_to body, url, classes
    else
      body
    end
  end
end
