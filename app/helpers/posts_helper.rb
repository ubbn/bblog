module PostsHelper
  def authorize_post_change
    unless admin_signed_in?
      @post = Post.find(params[:id])
      unless @post.user_id == current_user.id
        redirect_to :controller => "sessions", :action => "unauthorized"
      end
    end
  end

  def authorize_post_view
    unless admin_signed_in?
      @post = Post.find(params[:id])
      unless @post.is_public or @post.user_id == current_user.id
        redirect_to :controller => "sessions", :action => "unauthorized"
      end
    end
  end
end
