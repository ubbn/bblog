// This file is automatically compiled by Webpack, along with any other files
// present in this directory. You're encouraged to place your actual application logic in
// a relevant structure within app/javascript and only use these pack files to reference
// that code so it'll be compiled.

require('@rails/activestorage').start()
require('channels')
require('jquery')

// Uncomment to copy all static images under ../images to the output folder and reference
// them with the image_pack_tag helper in views (e.g <%= image_pack_tag 'rails.png' %>)
// or the `imagePath` JavaScript helper below.
//
const images = require.context('../images', true)
const imagePath = (name) => images(name, true)
import 'trix'
import '@rails/actiontext'
// This must be here for bootstrap responsive menu to work
// spent quite a lot of time to know it.
import 'bootstrap'
import '../src/application.css'
import '../src/actiontext.scss'
import '../images/favicon.ico'
import Rails from '@rails/ujs'
import Turbolinks from 'turbolinks'

Rails.start()
Turbolinks.start()
