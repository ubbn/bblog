class PostsController < ApplicationController
  include PostsHelper
  before_action :set_post, only: [:show, :edit, :update, :destroy]
  before_action :authenticate, only: [:new, :index, :create, :edit, :update, :destroy]
  before_action :authorize_post_change, only: [:edit, :destroy]
  before_action :authorize_post_view, only: [:show]

  # GET /posts
  # GET /posts.json
  def index
    if admin_signed_in?
      @posts = Post.all.order(created_at: :desc)
    elsif user_signed_in?
      @posts = Post.where(:user_id => current_user.id).order(created_at: :desc)
    else
      @posts = Post.where("is_public=? and published_at<?", true, DateTime.now).order(created_at: :desc)
    end
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
  end

  def facepage
    if admin_signed_in?
      @posts = Post.all
    elsif user_signed_in?
      @posts = Post.where("published_at<=?", DateTime.now)
    else
      @posts = Post.where("is_public=? and published_at<?", true, DateTime.now)
    end
  end

  # GET /posts/new
  def new
    @post = Post.new
  end

  # GET /posts/1/edit
  def edit
  end

  # POST /posts
  # POST /posts.json
  def create
    @post = Post.new(post_params)
    @post.user_id = current_user.id

    respond_to do |format|
      if @post.save
        format.html { redirect_to @post, notice: "Post was successfully created." }
        format.json { render :show, status: :created, location: @post }
      else
        format.html { render :new }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /posts/1
  # PATCH/PUT /posts/1.json
  def update
    respond_to do |format|
      if @post.update(post_params)
        format.html { redirect_to @post, notice: "Post was successfully updated." }
        format.json { render :show, status: :ok, location: @post }
      else
        format.html { render :edit }
        format.json { render json: @post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /posts/1
  # DELETE /posts/1.json
  def destroy
    @post.destroy
    respond_to do |format|
      format.html { redirect_to posts_url, notice: "Post was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_post
    @post = Post.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def post_params
    params.require(:post).permit(:title, :is_public, :published_at, :content)
  end
end
