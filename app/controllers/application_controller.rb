class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user, :user_signed_in?

  def authenticate
    redirect_to :controller => "sessions", :action => "unauthorized" unless user_signed_in?
  end

  def authorizeOnlyAdmin
    redirect_to :controller => "sessions", :action => "unauthorized" unless admin_signed_in?
  end

  def admin_signed_in?
    user_signed_in? and current_user.is_admin
  end

  def current_user
    @current_user ||= User.find(session[:user_id]) if session[:user_id]
  end

  def user_signed_in?
    # converts current_user to a boolean by negating the negation
    !!current_user
  end
end
