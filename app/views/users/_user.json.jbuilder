json.extract! user, :id, :provider, :uid, :email, :first_name, :last_name, :picture, :is_admin, :created_at, :updated_at
json.url user_url(user, format: :json)
